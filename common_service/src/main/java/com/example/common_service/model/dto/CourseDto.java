package com.example.common_service.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class CourseDto {
    private Long id;
    private String courseName;
    private String courseCode;
    private String description;
    private String instructor;
}
