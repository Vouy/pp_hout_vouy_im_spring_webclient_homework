package com.example.course_service.service.serviceImp;

import com.example.common_service.model.dto.CourseDto;
import com.example.course_service.model.entity.Course;
import com.example.course_service.repository.CourseRepositry;
import com.example.course_service.service.CourseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CourseServiceImp implements CourseService {

    private final CourseRepositry courseRepositry;
    @Override
    public List<CourseDto> getAllCourse() {
        return courseRepositry.findAll().stream().map(Course::toDto).toList();
    }

    @Override
    public CourseDto getCourseById(Long Id) {
        return courseRepositry.findById(Id).get().toDto();
    }


}
