package com.example.course_service.service;
import com.example.common_service.model.dto.CourseDto;
import java.util.List;

public interface CourseService {
    List<CourseDto> getAllCourse();
    CourseDto getCourseById(Long Id);
}
