package com.example.course_service.controller;

import com.example.common_service.model.dto.CourseDto;
import com.example.course_service.service.CourseService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
@AllArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @GetMapping("course")
    public List<CourseDto> getAllCourse(){
        return courseService.getAllCourse();
    }

    @GetMapping("course/{id}")
    public CourseDto getCourseById(@PathVariable Long id){
        return courseService.getCourseById(id);
    }


}
