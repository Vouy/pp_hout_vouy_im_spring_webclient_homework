package com.example.course_service.repository;

import com.example.course_service.model.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepositry extends JpaRepository<Course, Long> {
}
