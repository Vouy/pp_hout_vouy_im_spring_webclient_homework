package com.example.course_service.model.entity;

import com.example.common_service.model.dto.CourseDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String courseName;
    private String courseCode;
    private String description;
    private String instructor;

    public CourseDto toDto() {
        return new CourseDto(this.id,this.courseName,this.courseCode,this.description,this.instructor);
    }

}

