package com.example.student_service.service;

import com.example.common_service.model.dto.CourseDto;
import com.example.student_service.model.dto.StudentDto;
import com.example.student_service.model.entity.Student;
import com.example.student_service.model.request.StudentRequest;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

public interface StudentService {
    List<StudentDto> getAllStudent();
//    Mono<CourseDto> getCourseById(Long id);

    StudentDto getStudentById(Long id);

    StudentDto deleteStudent(Long id);

    StudentDto addStudent(StudentRequest studentRequest);

    StudentDto updateStudent(Long Id, StudentRequest studentRequest);
}


