package com.example.student_service.service.serviceImp;

import com.example.common_service.model.dto.CourseDto;
import com.example.student_service.model.dto.StudentDto;
import com.example.student_service.model.entity.Student;
import com.example.student_service.model.request.StudentRequest;
import com.example.student_service.repository.StudentRepository;
import com.example.student_service.service.StudentService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;


import java.util.ArrayList;
import java.util.List;

@Service
//@AllArgsConstructor
public class StudentServiceImp implements StudentService {

    private final StudentRepository studentRepository;
    private final WebClient webClient;

    public StudentServiceImp(StudentRepository studentRepository, WebClient.Builder webClientBuilder) {
        this.studentRepository = studentRepository;
        this.webClient = webClientBuilder.baseUrl("http://localhost:8081/api/v1/").build();
    }

    public Mono<CourseDto> getCourseById(Long id){
        return webClient.get()
                .uri("course/{id}",id)
                .retrieve()
                .bodyToMono(CourseDto.class);
    }

    @Override
    public List<StudentDto> getAllStudent() {
        var  student =  studentRepository.findAll();
        List<StudentDto> studentDto = new ArrayList<>();
        for(Student stu : student){
            var course =  getCourseById(stu.getCourse_id()).block();
//                studentDto.setCourse(course);
//                stu.toDto(course);
                studentDto.add(stu.toDto(course));

        }
        return studentDto;

    }

    @Override
    public StudentDto getStudentById(Long id) {

        StudentDto studentDto = new StudentDto();

        var student = studentRepository.findById(id);
        var course  = getCourseById(student.get().getCourse_id()).block();
        studentDto.setId(student.get().getId());
        studentDto.setFirstName(student.get().getFirstName());
        studentDto.setLastName(student.get().getLastName());
        studentDto.setDescription(student.get().getDescription());
        studentDto.setEmail(student.get().getEmail());
        studentDto.setDataBirth(student.get().getDataBirth());
        studentDto.setCourse(course);

        return studentDto;
    }


    @Override
    public StudentDto deleteStudent(Long id) {
        Student stuId = studentRepository.findById(id).orElse(null);
        if(stuId != null){
            studentRepository.deleteById(stuId.getId());
        }
        return null;
    }

    @Override
    public StudentDto addStudent(StudentRequest studentRequest) {
        var studentEntity = studentRequest.toEntity();
//        var courseId = studentRequest.getCourse_id();
        var course = getCourseById(studentRequest.getCourse_id()).block();
        return studentRepository.save(studentEntity).toDto(course);
    }

    @Override
    public StudentDto updateStudent(Long Id, StudentRequest studentRequest) {
        Student stuId = studentRepository.findById(Id).orElse(null);
        var course = getCourseById(studentRequest.getCourse_id()).block();
        if(stuId != null){
            var studentEntity = studentRequest.toEntityById(stuId.getId());
            return studentRepository.save(studentEntity).toDto(course);
        }
        return null;
    }


}
