package com.example.student_service.controller;

import com.example.student_service.model.dto.StudentDto;
import com.example.student_service.model.request.StudentRequest;
import com.example.student_service.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
@AllArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @GetMapping("student")
    public List<StudentDto> getAllStudent(){
        return studentService.getAllStudent();
    }

    @GetMapping("student/{id}")
    public StudentDto getStudentById(@PathVariable Long id){
        return studentService.getStudentById(id);
    }

    @DeleteMapping("student/{id}")
    public StudentDto deleteStudent(@PathVariable Long id){
        return studentService.deleteStudent(id);
    }

    @PostMapping("sudent")
    public StudentDto addStudent(@RequestBody StudentRequest studentRequest){
        return studentService.addStudent(studentRequest);
    }

    @PutMapping("student/{id}")
    public StudentDto updateStudent(@RequestParam Long id, @RequestBody StudentRequest studentRequest){
        return studentService.updateStudent(id,studentRequest);
    }
}
