package com.example.student_service.model.dto;

import com.example.common_service.model.dto.CourseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class StudentDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String description;
    private String email;
    private LocalDateTime dataBirth;
    private CourseDto course;


}
