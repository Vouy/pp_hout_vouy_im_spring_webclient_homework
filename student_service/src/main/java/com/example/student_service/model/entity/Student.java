package com.example.student_service.model.entity;

import com.example.common_service.model.dto.CourseDto;
import com.example.student_service.model.dto.StudentDto;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
//@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String description;
    private String email;
    private LocalDateTime dataBirth;
    private Long course_id;

    public Student(String firstName, String lastName, String description, String email, LocalDateTime dataBirth, Long courseId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.email = email;
        this.dataBirth = dataBirth;
        this.course_id = courseId;
    }

    public Student(Long id,String firstName, String lastName, String description, String email, LocalDateTime dataBirth, Long courseId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.email = email;
        this.dataBirth = dataBirth;
        this.course_id = courseId;
    }

    public StudentDto toDto(CourseDto courseDto){
//        return new StudentDto(this.id,this.firstName,this.lastName,this.description,this.email,this.dataBirth);
        StudentDto studentDto = new StudentDto();
        studentDto.setId(id);
        studentDto.setFirstName(firstName);
        studentDto.setLastName(lastName);
        studentDto.setDescription(description);
        studentDto.setEmail(email);
        studentDto.setDataBirth(dataBirth);
        studentDto.setCourse(courseDto);

        return studentDto;
    }



}
