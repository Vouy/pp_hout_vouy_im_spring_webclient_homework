package com.example.student_service.model.request;

import com.example.student_service.model.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class StudentRequest {
    private String firstName;
    private String lastName;
    private String description;
    private String email;
    private LocalDateTime dataBirth;
    private Long course_id;

    public Student toEntity(){
        return new Student(this.firstName,this.lastName,this.description,this.email,this.dataBirth,this.course_id);
    }

    public Student toEntityById(Long id){
        return new Student(id,this.firstName,this.lastName,this.description,this.email,this.dataBirth,this.course_id);
    }
}
