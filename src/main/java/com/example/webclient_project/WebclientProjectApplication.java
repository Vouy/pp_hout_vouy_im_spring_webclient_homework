package com.example.webclient_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebclientProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebclientProjectApplication.class, args);
	}

}
